import unittest
import chiffre_cesar


class TestChiffreCesarFunctions(unittest.Testcase):

    def test_chiffrer(chiffrer):
        self.assertEqual(chiffre_cesar.chiffrer("abc", 13),"nop")
        self.assertEqual(chiffre_cesar.chiffrer("abc", 8),"ijk")
    
    def test_dechiffrer(dechiffrer):
        self.assertEqual(chiffre_cesar.dechiffrer("nop", 13),"abc")
        self.assertEqual(chiffre_cesar.dechiffrer("ijk", 8),"abc")
