from array import array
def chiffrer(mot, CLE):
    nouveau_mot = ""
    for lettre in mot :
        nouveau_mot += chr(ord(lettre)+CLE)            
    return nouveau_mot

def dechiffrer(mot, CLE):
    nouveau_mot = ""
    for lettre in mot :
        nouveau_mot += chr(ord(lettre)-CLE)     
    return nouveau_mot



