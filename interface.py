from tkinter import *
from tkinter.messagebox import showinfo
import sys
import chiffre_cesar
import string


def button_chiffrer_callback():
    mot = texte.get()
    showinfo(title="chiffrer",
             message='%s' % chiffre_cesar.chiffrer(mot, CLE))

def button_dechiffrer_callback():
    mot = texte.get()
    showinfo(title="dechiffrer",
             message='%s' % chiffre_cesar.dechiffrer(mot, CLE))

if __name__ == '__main__':
    
    frame = Tk()
    frame.title("Chiffre de Cesar")
    frame.rowconfigure(0, weight=1)
    frame.rowconfigure(1, weight=1)
    frame.rowconfigure(2, weight=1)
    frame.rowconfigure(3, weight=1)    
    frame.rowconfigure(4, weight=1)

    frame.columnconfigure(0, weight=1)
    frame.columnconfigure(1, weight=1)

    texte = StringVar()
    texte.set("abc")

    label_zoneTexte = Label ( frame, text = "Entrez le texte à chiffrer / déchiffrer" )
    label_zoneTexte.grid(row=0, columnspan=3, padx=10, pady=10, sticky='nsew')
    
    zoneTexte = Entry(frame, textvariable=texte)
    zoneTexte.grid(row=1, columnspan=3, padx=10, pady=10, sticky='nsew')

    label_cle = Label ( frame, text = "Clé :" )    
    label_cle.grid(row=2, columnspan=3, padx=10, pady=10, sticky='nsew')

    CLE = IntVar()
    CLE.set(13)
    txt_CLE = Entry(frame, textvariable=CLE)
    txt_CLE.grid(row=3, columnspan=3, padx=10, pady=10, sticky='nsew')

    btn_chiffrer = Button(frame,
                             text='Chiffrer',
                             command=button_chiffrer_callback)
    btn_chiffrer.grid(row=4, column=0)

    btn_dechiffrer = Button(frame,
                             text='Dechiffrer',
                             command=button_dechiffrer_callback)
    btn_dechiffrer.grid(row=4, column=1) 
    frame.mainloop()
    
    